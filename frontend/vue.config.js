/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    // options...
    devServer: {
        proxy: {
            '/api': {
                target: 'http://localhost:8098', // this configuration needs to correspond to the Spring Boot backends' application.properties server.port
                ws: true,
                changeOrigin: true
            }
        }
    },
    // Change build paths to make them Maven compatible
    // see https://cli.vuejs.org/config/
    outputDir: 'target/dist',
    assetsDir: 'static'

};
