package fr.efrei.securidocs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecuridocsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecuridocsApplication.class, args);
	}

}
